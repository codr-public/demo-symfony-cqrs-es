<?php

namespace App\Tests\Controller\Employee\Command;

use App\Controller\Employee\Command\RegisterEmployeeCommandController;
use App\Event\RegistredEmployeeCommandEvent;
use App\Tests\CQRS\Controller\CommandControllerKernelTestCase;
use ReflectionException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegisterEmployeeCommandControllerTest extends CommandControllerKernelTestCase
{
    /**
     * @throws ReflectionException
     */
    public function testPOSTRegisterEmployee(): void
    {
        $request = $this->createRequest('register_employee', [
            'username' => 'value',
            'business_unit' => 'bar',
        ]);

        $this->expectEvents([
            [
                RegistredEmployeeCommandEvent::class,
                [
                    'username' => 'value',
                    'businessUnit' => 'bar',
                ],
            ],
        ]);

        $response = $this->invokeController(RegisterEmployeeCommandController::class, $request);

        $this->assertEquals(RedirectResponse::class, get_class($response));
    }
}
