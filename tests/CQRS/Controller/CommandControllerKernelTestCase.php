<?php

namespace App\Tests\CQRS\Controller;

use App\CQRS\Controller\AbstractCommandController;
use App\CQRS\Repository\AbstractInMemoryRepositoryInterface;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CommandControllerKernelTestCase extends KernelTestCase
{
    protected Container $container;
    protected SerializerInterface $serializer;
    protected AbstractInMemoryRepositoryInterface $eventRepository;
    protected EventDispatcherInterface $dispatcher;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->container = static::getContainer();

        $this->serializer = $this->container->get(SerializerInterface::class);
        $this->eventRepository = $this->createMock(AbstractInMemoryRepositoryInterface::class);
        $this->dispatcher = $this->createMock(EventDispatcherInterface::class);
    }

    protected function createRequest(string $formName, array $formValues, $method = 'POST'): Request
    {
        $request = new Request();
        $request->setMethod($method);
        $request->request->set($formName, $formValues);

        return $request;
    }

    private function createController(string $className): AbstractCommandController {
        $controller = new $className();
        $controller->setContainer($this->container);

        return $controller;
    }

    /**
     * @throws ReflectionException
     */
    protected function invokeController(string $className, Request $request): Response
    {
        $controller = $this->createController($className);

        return $controller(
            $request,
            $this->serializer,
            $this->eventRepository,
            $this->dispatcher,
        );
    }

    protected function expectEvents(array $expectedEvents): void
    {
        $matcher = $this->exactly(count($expectedEvents));
        $this->dispatcher
            ->expects($matcher)
            ->method('dispatch')
            ->with(
                $this->callback(function ($params) use ($matcher, $expectedEvents) {
                    $expected = $expectedEvents[$matcher->getInvocationCount() - 1];

                    $this->assertEquals($expected[0], get_class($params));

                    foreach ($expected[1] as $key => $value) {
                        $this->assertEquals($value, $params->$key);
                    }

                    return true;
                })
            )
        ;
    }
}
