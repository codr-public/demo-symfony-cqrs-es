<?php

namespace App\EventHandler\Employee;

use App\CQRS\Attribute\AsCQRSEventHandler;
use App\CQRS\Event\CommandEventInterface;
use App\CQRS\EventHandler\AbstractCommandEventHandler;
use App\Entity\Employee;
use App\Event\RegistredEmployeeCommandEvent;
use App\Repository\EmployeeRepository;

#[AsCQRSEventHandler(eventClass: RegistredEmployeeCommandEvent::class)]
readonly class CreateEmployeeEventHandler extends AbstractCommandEventHandler {

    public function __construct(
        protected EmployeeRepository $repository,
    ) {
    }

    /**
     * @param RegistredEmployeeCommandEvent $event
     * @return void
     */
    public function handle(CommandEventInterface $event): void
    {
        $entity = new Employee();
        $entity
            ->setUuid($event->id)
            ->setUsername($event->username)
            ->setBusinessUnit($event->businessUnit)
        ;

        $this->repository->save($entity);
    }
}