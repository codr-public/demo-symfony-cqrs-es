<?php

namespace App\EventHandler\User;

use App\CQRS\Attribute\AsCQRSEventHandler;
use App\CQRS\Event\CommandEventInterface;
use App\CQRS\EventHandler\AbstractCommandEventHandler;
use App\Entity\User;
use App\Event\RegistredEmployeeCommandEvent;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCQRSEventHandler(eventClass: RegistredEmployeeCommandEvent::class)]
readonly class CreateNewUserEventHandler extends AbstractCommandEventHandler {

    public function __construct(
        protected UserRepository $repository,
        protected UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    /**
     * @param RegistredEmployeeCommandEvent $event
     * @return void
     */
    public function handle(CommandEventInterface $event): void
    {
        $entity = new User();

        $password = $this->passwordHasher->hashPassword($entity, "user");

        $entity
            ->setUuid($event->id)
            ->setUsername($event->username)
            ->setPassword($password)
            ->setRoles(['ROLE_USER'])
        ;

        $this->repository->save($entity);
    }
}