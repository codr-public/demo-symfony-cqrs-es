<?php

namespace App\Event;

use App\CQRS\Event\CommandEventInterface;
use Symfony\Component\Uid\Uuid;

readonly class RegistredEmployeeCommandEvent implements CommandEventInterface {
    public function __construct(
        public Uuid $id,
        public string $username,
        public string $businessUnit,
    ) {
    }
}