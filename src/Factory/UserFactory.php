<?php

namespace App\Factory;

use App\Entity\User;
use Symfony\Component\Uid\Uuid;

class UserFactory {
    public static function getAnon(): User {
        $uuid = Uuid::fromString("00000000-0000-0000-0000-000000000000");
        return new User($uuid);
    }

    public static function getCli(): User {
        $uuid = Uuid::fromString("42424242-4242-4242-4242-424242424242");
        return new User($uuid);
    }
}