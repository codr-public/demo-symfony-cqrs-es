<?php

namespace App\CQRS\Event;

interface CommandEventInterface {
    const PERSISTED = "command.persisted";
}