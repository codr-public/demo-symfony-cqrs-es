<?php

namespace App\CQRS\Repository;

use App\CQRS\Entity\EntityInterface;

interface RepositoryInterface {
    public function save(EntityInterface $entity): void;
}