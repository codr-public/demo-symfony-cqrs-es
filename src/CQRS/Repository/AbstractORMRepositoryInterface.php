<?php

namespace App\CQRS\Repository;

use App\CQRS\Entity\EntityInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

abstract class AbstractORMRepositoryInterface  extends ServiceEntityRepository implements RepositoryInterface {

    public function save(EntityInterface $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }
}