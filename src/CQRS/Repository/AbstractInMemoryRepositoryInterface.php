<?php

namespace App\CQRS\Repository;

use App\CQRS\Entity\EntityInterface;

abstract class AbstractInMemoryRepositoryInterface  implements RepositoryInterface {

    private array $entities = [];

    public function save(EntityInterface $entity): void
    {
        $this->entities[] = $entity;
    }

    public function getEntities(): array
    {
        return $this->entities;
    }
}