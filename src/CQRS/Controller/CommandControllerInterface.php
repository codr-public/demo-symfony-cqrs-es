<?php

namespace App\CQRS\Controller;

use App\CQRS\Entity\EntityInterface;
use App\CQRS\Event\CommandEventInterface;
use Symfony\Component\HttpFoundation\Response;

interface CommandControllerInterface {
    public function isAuthorized(): bool;
    public function validate(): bool;
    public function prepareEntity(): EntityInterface;
    /**
     * @return iterable<CommandEventInterface>
     */
    public function prepareEvents(): iterable;

    /**
     * @param iterable<CommandEventInterface> $events
     */
    public function prepareResponse(iterable $events): Response;
}
