<?php

namespace App\CQRS\Controller;

use App\CQRS\Attribute\AsCQRSCommand;
use App\CQRS\Entity\EntityInterface;
use App\CQRS\Event\CommandEventInterface;
use App\CQRS\Repository\RepositoryInterface;
use App\Entity\Event;
use App\Factory\UserFactory;
use DateTimeImmutable;
use LogicException;
use ReflectionClass;
use ReflectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

abstract class AbstractCommandController extends AbstractController implements CommandControllerInterface{
    protected EntityInterface $entity;
    protected AsCQRSCommand $attribute;

    /**
     * @throws ReflectionException
     */
    public function __invoke(
        Request $request,
        SerializerInterface $serializer,
        RepositoryInterface $eventRepository,
        EventDispatcherInterface $dispatcher,
    ): Response
    {
        $this->readAttribute();

        // Prepare entity
        $this->entity = $this->prepareEntity();

        // Prepare form and validate it
        $form = $this->createForm($this->attribute->formClass, $this->entity);
        $form->handleRequest($request);

        if(!$form->isValid() || !$this->validate()) {
            throw new BadRequestHttpException('Invalid data');
        }

        if (!$this->isAuthorized()) {
            throw new AccessDeniedHttpException();
        }

        if ('cli' === PHP_SAPI) {
            $user = UserFactory::getCli();
        } else {
            $user = $this->getUser();

            if(!$user) {
                $user = UserFactory::getAnon();
            }
        }

        $dispatchedEvents = $this->prepareEvents();

        foreach ($dispatchedEvents as $dispatchedEvent) {
            $date = new DateTimeImmutable();
            $payload = $serializer->serialize($dispatchedEvent, 'json');

            $storedEvent = new Event(Uuid::v4());
            $storedEvent
                ->setUserId($user->getUuid())
                ->setDatetime($date)
                ->setMicrosec($date->format('u'))
                ->setEventClass($dispatchedEvent::class)
                ->setEventPayload($payload)
            ;

            $eventRepository->save($storedEvent);

            $dispatcher->dispatch($dispatchedEvent, CommandEventInterface::PERSISTED);
        }

        return $this->prepareResponse($dispatchedEvents);
    }

    /**
     * @return EntityInterface of class $className
     */
    public function prepareEntity(): EntityInterface
    {
        $className = $this->attribute->entityClass;

        /** @var EntityInterface $entity */
        $entity = new $className();
        $entity->setUuid(Uuid::v4());

        return $entity;
    }

    public function isAuthorized(): bool {
        return true;
    }

    public function validate(): bool {
        return true;
    }

    /**
     * @throws ReflectionException
     */
    private function readAttribute(): void
    {
        $class = static::class;

        $attributes = (new ReflectionClass($class))->getAttributes(AsCQRSCommand::class);

        if (empty($attributes)) {
            throw new LogicException('CommandControllers must be annotated with AsCQRSCommand.', 0);
        }

        $this->attribute = $attributes[0]->newInstance();
    }
}
