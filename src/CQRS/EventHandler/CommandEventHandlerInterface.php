<?php

namespace App\CQRS\EventHandler;

use App\CQRS\Event\CommandEventInterface;

interface CommandEventHandlerInterface
{
    public function handle(CommandEventInterface $event): void;
    public function supported(CommandEventInterface $event): bool;
}
