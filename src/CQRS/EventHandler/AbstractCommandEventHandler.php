<?php

namespace App\CQRS\EventHandler;

use App\CQRS\Attribute\AsCQRSEventHandler;
use App\CQRS\Event\CommandEventInterface;
use LogicException;
use ReflectionClass;
use ReflectionException;

readonly abstract class AbstractCommandEventHandler implements CommandEventHandlerInterface {
    /**
     * @throws ReflectionException
     */
    public function supported(CommandEventInterface $event): bool
    {
        $class = static::class;

        $attributes = (new ReflectionClass($class))->getAttributes(AsCQRSEventHandler::class);

        if (empty($attributes)) {
            throw new LogicException('CommandControllers must be annotated with AsCQRSCommand.', 0);
        }

        /** @var AsCQRSEventHandler $attribute */
        $attribute = $attributes[0]->newInstance();

        return get_class($event) === $attribute->eventClass;
    }

    abstract public function handle(CommandEventInterface $event): void;
}