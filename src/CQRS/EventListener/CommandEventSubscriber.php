<?php

namespace App\CQRS\EventListener;

use App\CQRS\Event\CommandEventInterface;
use App\CQRS\EventHandler\CommandEventHandlerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

#[AsEventListener]
readonly class CommandEventSubscriber implements EventSubscriberInterface {
    /**
     * @param iterable<CommandEventHandlerInterface> $handlers
     */
    public function __construct(private iterable $handlers) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CommandEventInterface::PERSISTED => 'onCommandPersisted',
        ];
    }

    public function onCommandPersisted(CommandEventInterface $event): void
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supported($event)) {
                $handler->handle($event);
            }
        }
    }
}