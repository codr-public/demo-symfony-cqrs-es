<?php

namespace App\CQRS\Attribute;

#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::IS_REPEATABLE)]
class AsCQRSEventHandler {
    public function __construct(
        public string $eventClass,
    ) {

    }
}
