<?php

namespace App\CQRS\Attribute;

#[\Attribute(\Attribute::TARGET_CLASS)]
class AsCQRSCommand {
    public function __construct(
        public string $formClass,
        public string $entityClass,
    ) {

    }
}
