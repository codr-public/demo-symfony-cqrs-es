<?php

namespace App\CQRS\Entity;

use Symfony\Component\Uid\Uuid;

interface EntityInterface {
    public function getUuid(): ?Uuid;
    public function setUuid(Uuid $uuid): static;
}