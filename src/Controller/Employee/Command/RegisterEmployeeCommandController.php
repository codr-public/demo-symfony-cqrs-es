<?php

namespace App\Controller\Employee\Command;

use App\CQRS\Attribute\AsCQRSCommand;
use App\CQRS\Controller\AbstractCommandController;
use App\Entity\Employee;
use App\Event\RegistredEmployeeCommandEvent;
use App\Form\RegisterEmployeeType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @property Employee $entity
 */
#[Route('/register', name: 'security_register_employee_command', methods: ['POST'])]
#[AsCQRSCommand(formClass: RegisterEmployeeType::class, entityClass: Employee::class)]
class RegisterEmployeeCommandController extends AbstractCommandController
{
    public function prepareEvents(): iterable
    {
        return [
            new RegistredEmployeeCommandEvent(
                $this->entity->getUuid(),
                $this->entity->getUsername(),
                $this->entity->getbusinessUnit(),
            ),
        ];
    }

    /**
     * @param iterable<RegistredEmployeeCommandEvent> $events
     */
    public function prepareResponse(iterable $events): Response
    {
        return $this->redirectToRoute('home');
    }
}
