<?php

namespace App\Controller\Employee\Query;

use App\Entity\Employee;
use App\Form\RegisterEmployeeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/register', name: 'security_register_employee_query', methods: ['GET'])]
class RegisterEmployeeQueryController extends AbstractController
{
    public function __invoke(): Response
    {
        return $this->render('domain/security/register.html.twig', [
            'form' => $this->createForm(RegisterEmployeeType::class, new Employee())->createView(),
        ]);
    }
}
