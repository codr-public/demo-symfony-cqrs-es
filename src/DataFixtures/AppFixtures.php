<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Uid\Uuid;

class AppFixtures extends Fixture
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly UserRepository $userRepository,
    ) {

    }

    public function load(ObjectManager $manager): void
    {
        $anon = new User();
        $anon
            ->setUuid(Uuid::fromString("00000000-0000-0000-0000-000000000000"))
            ->setUsername("anonymous")
            ->setPassword("")
            ->setRoles(['ROLE_ANONYMOUS']);

        $this->userRepository->save($anon);

        //

        $admin = new User();

        $password = $this->passwordHasher->hashPassword($admin, "admin");

        $admin
            ->setUuid(Uuid::v4())
            ->setUsername("admin")
            ->setPassword($password)
            ->setRoles(['ROLE_ADMIN']);

        $this->userRepository->save($admin);
    }
}
