<?php

namespace App\Entity;

use App\CQRS\Entity\EntityInterface;
use App\Repository\EventRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event implements EntityInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private ?Uuid $uuid = null;

    #[ORM\Column(type: 'uuid')]
    private ?Uuid $userId = null;

    #[ORM\Column(length: 255)]
    private ?string $eventClass = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $eventPayload = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $datetime = null;

    #[ORM\Column]
    private ?int $microsec = null;

    public function __construct(Uuid $uuid = null)
    {
        if($uuid) {
            $this->setUuid($uuid);
        }
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function setUuid(Uuid $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUserId(): ?Uuid
    {
        return $this->userId;
    }

    public function setUserId(Uuid $userId): static
    {
        $this->userId = $userId;

        return $this;
    }

    public function getEventClass(): ?string
    {
        return $this->eventClass;
    }

    public function setEventClass(string $eventClass): static
    {
        $this->eventClass = $eventClass;

        return $this;
    }

    public function getEventPayload(): ?string
    {
        return $this->eventPayload;
    }

    public function setEventPayload(string $eventPayload): static
    {
        $this->eventPayload = $eventPayload;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): static
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getMicrosec(): ?int
    {
        return $this->microsec;
    }

    public function setMicrosec(int $microsec): static
    {
        $this->microsec = $microsec;

        return $this;
    }
}
