
# CQRS and ES

## A simple demo in symfony

A small poc of what could be done to implements all of that.


## Prerequisites

* php 8.2
* composer
* symfony CLI
* docker
* docker compose

```bash
symfony check:req
```

## Run Locally

Clone the project

```bash
git clone https://gitlab.com/codr-public/demo-symfony-cqrs-es.git
```

Go to the project directory

```bash
cd demo-symfony-cqrs-es
```

Install dependencies

```bash
composer install
```

Start the containers

```bash
docker compose up -d
```

Start the server

```bash
symfony server:start
```
## Related

[Julien Vinber - AddressBook demo](https://gitlab.com/julienVinber/addressbook-cqrs-eventsourcing-symfony-demo)


## Authors

- [@gaveline](https://www.linkedin.com/in/aveline-guillaume/)


## Acknowledgements

- [CQRS et Event Sourcing par l'exemple](https://gitlab.com/JulienVinberSlide/cqrs-et-event-sourcing-par-l-exemple)

- [CQRS & Events Sourcing avec GCP (1/3) : Introduction](https://medium.com/codeshake/cqrs-events-sourcing-avec-gcp-1-3-introduction-e5a62bbdae28)
